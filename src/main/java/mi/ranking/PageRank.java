package mi.ranking;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;
import mi.analyzer.model.Connection;
import mi.analyzer.model.Node;
import mi.analyzer.model.WebGraph;

import java.io.File;
import java.io.IOException;
import java.util.*;

import static com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility.NONE;

@JsonAutoDetect(fieldVisibility = NONE, getterVisibility = NONE, setterVisibility = NONE)
public class PageRank {
    @JsonProperty("dampening")
    private double dampening;
    private WebGraph graph;
    private HashMap<Node, Double> ranks;

    public PageRank(WebGraph graph, double dampening){
        this.dampening = dampening;
        this.graph = graph;
        initRanks();
    }

    public void iterate(){
        int n = graph.getNodes().size();
        for(Node node: graph.getNodes()){
            double rank = dampening / n + (1.0 - dampening) * computeRank(node);
            ranks.put(node, rank);
        }
    }

    private double computeRank(Node node) {
        List<Connection> parents = node.getIn();
        double sum = 0;
        for(Connection parent : parents){
            Node parentNode = parent.getNode();
            int parentOuts = parentNode.getOutDegree();
            if(parentNode != node){
                sum += ranks.get(parentNode) / parentOuts;
            }
        }
        return sum;
    }

    private void initRanks() {
        this.ranks = new HashMap<>();
        int size = graph.getNodes().size();
        for(Node node : graph.getNodes()){
            ranks.put(node, 1.0 / size);
        }
    }

    @JsonProperty("ranks")
    public List<PageRankNode> getSimpleRanks(){
        List<PageRankNode> result = new ArrayList<>();

        for (Map.Entry<Node, Double> entry : ranks.entrySet()) {
            result.add(new PageRankNode(entry.getKey().getUrl(), entry.getValue()));
        }

        result.sort(Comparator.comparing(PageRankNode::getRank));

        return result;
    }

    public HashMap<Node, Double> getRanks(){
        return ranks;
    }

    public double getDampening(){
        return dampening;
    }

    public void saveAsJson(String path) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        mapper.writerWithDefaultPrettyPrinter().writeValue(new File(path), this);
    }
}
