package mi.ranking;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

import static com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility.ANY;
import static com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility.NONE;

@JsonAutoDetect(fieldVisibility = ANY, getterVisibility = NONE, setterVisibility = NONE)
public class PageRankNode {
    private String url;
    private Double rank;

    public PageRankNode(){}

    public PageRankNode(String url, Double rank){
        this.url = url;
        this.rank = rank;
    }

    public Double getRank() {
        return rank;
    }

    public String getUrl() {
        return url;
    }
}
