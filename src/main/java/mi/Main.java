package mi;

import mi.analyzer.model.WebGraph;
import mi.crawler.model.CrawlerData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URISyntaxException;

public class Main {
    private final static Logger LOGGER = LoggerFactory.getLogger(Main.class);

    public static void main(String [] args) throws IOException, URISyntaxException {
        String url = args[0];
        String destination = args[1];
        int threads = Integer.valueOf(args[2]);
        int maxSites = Integer.valueOf(args[3]);
        boolean loadFromFile = args.length == 5 && Boolean.TRUE.equals(Boolean.valueOf(args[4]));
        int iterations = 20;

        LOGGER.info("PARAMETERS: URL => {}, DESTINATION => {}, THREADS => {}, MAX SITES => {}, LOAD FROM FILE => {}",
                url, destination, threads, maxSites, loadFromFile);

        Tester tester = new Tester(url, destination);
        CrawlerData data = loadFromFile ? tester.reloadCrawlerDataFromFile() : tester.crawlSite(threads, maxSites);
        WebGraph webGraph = tester.createWebAnalyzer(data);
        //tester.computeStatsForWebGraph(webGraph);
        //tester.computePR(iterations, webGraph);
        tester.damageGraph(webGraph);
    }
}
