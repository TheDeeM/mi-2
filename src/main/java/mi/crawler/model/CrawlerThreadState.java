package mi.crawler.model;

public enum CrawlerThreadState {
    SLEEPING, WAKING_UP, WORKING, GOING_TO_SLEEP;
}
