package mi.crawler.model;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.io.IOException;

import static com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility.ANY;
import static com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility.NONE;

@JsonAutoDetect(fieldVisibility = ANY, getterVisibility = NONE, setterVisibility = NONE)
public class CrawlerStats {
    private double averageDownloadTime;
    private long workingTime;
    private long errorsDuringDownload;
    private long disallowedBecauseOfRobotsTxt;
    private long notInTheSameDomain;
    private long downloadedPages;
    private long minLoadTime;
    private long maxLoadTime;
    private int foundRobotsRules;
    private long leftToVisit;

    public double getAverageDownloadTime() {
        return averageDownloadTime;
    }

    public void setAverageDownloadTime(double averageDownloadTime) {
        this.averageDownloadTime = averageDownloadTime;
    }

    public long getWorkingTime() {
        return workingTime;
    }

    public void setWorkingTime(long workingTime) {
        this.workingTime = workingTime;
    }

    public long getErrorsDuringDownload() {
        return errorsDuringDownload;
    }

    public void setErrorsDuringDownload(long errorsDuringDownload) {
        this.errorsDuringDownload = errorsDuringDownload;
    }

    public long getDisallowedBecauseOfRobotsTxt() {
        return disallowedBecauseOfRobotsTxt;
    }

    public void setDisallowedBecauseOfRobotsTxt(long disallowedBecauseOfRobotsTxt) {
        this.disallowedBecauseOfRobotsTxt = disallowedBecauseOfRobotsTxt;
    }

    public long getNotInTheSameDomain() {
        return notInTheSameDomain;
    }

    public void setNotInTheSameDomain(long notInTheSameDomain) {
        this.notInTheSameDomain = notInTheSameDomain;
    }

    public void saveAsJson(String path) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        mapper.writerWithDefaultPrettyPrinter().writeValue(new File(path), this);
    }

    public long getDownloadedPages() {
        return downloadedPages;
    }

    public void setDownloadedPages(long downloadedPages) {
        this.downloadedPages = downloadedPages;
    }

    public long getMinLoadTime() {
        return minLoadTime;
    }

    public void setMinLoadTime(long minLoadTime) {
        this.minLoadTime = minLoadTime;
    }

    public long getMaxLoadTime() {
        return maxLoadTime;
    }

    public void setMaxLoadTime(long maxLoadTime) {
        this.maxLoadTime = maxLoadTime;
    }

    public int getFoundRobotsRules() {
        return foundRobotsRules;
    }

    public void setFoundRobotsRules(int foundRobotsRules) {
        this.foundRobotsRules = foundRobotsRules;
    }

    public long getLeftToVisit() {
        return leftToVisit;
    }

    public void setLeftToVisit(long leftToVisit) {
        this.leftToVisit = leftToVisit;
    }
}
