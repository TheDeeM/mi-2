package mi.crawler.model;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility.ANY;
import static com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility.NONE;

@JsonAutoDetect(fieldVisibility = ANY, getterVisibility = NONE, setterVisibility = NONE)
public class CrawlerData implements Serializable{
    private URI url;
    private List<Site> sites;
    private long workingTime;

    public CrawlerData(){}

    public void init(String url) throws URISyntaxException {
        workingTime = new Date().getTime();
        sites = new ArrayList<>();
        this.url = new URI(url);
    }

    public void addSite(Site site) {
        this.sites.add(site);
    }

    public void saveJsonToFile(String file) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        mapper.writerWithDefaultPrettyPrinter().writeValue(new File(file), this);
    }

    public void stop() {
        this.workingTime = new Date().getTime() - this.workingTime;
    }

    public URI getUrl() {
        return url;
    }

    public void setUrl(URI url) {
        this.url = url;
    }

    public List<Site> getSites(){
        return this.sites;
    }

    public long getWorkingTime(){
        return this.workingTime;
    }

    public Site findSite(String url){
        Site result = null;
        if(url != null){
            for(Site site : sites){
                if(url.equals(site.getUrl())){
                    result = site;
                    break;
                }
            }
        }

        return result;
    }
}
