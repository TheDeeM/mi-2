package mi.crawler.model;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CrawlerRules {
    private static List<String> COMMON_DOMAINS = Arrays.asList("edu", "com", "org", "co", "uk", "ac", "pl", "gov");
    private final static String DISALLOW_RULE = "Disallow: ";
    private final static List<String> DISALLOWED_EXTENSIONS = Arrays.asList(".pdf", ".xml");
    private final Logger LOGGER = LoggerFactory.getLogger(getClass());

    private List<String> disallowRules;

    private int declinedUrlFromRobots;
    private int declinedUrlFromDomain;

    public CrawlerRules(BufferedReader robotsBuffer){
        disallowRules = new ArrayList<>();
        declinedUrlFromRobots = 0;
        if(robotsBuffer != null){
            initRobotRules(robotsBuffer);
        }
    }

    public boolean shouldVisitUrl(String url){
        boolean result;
        try {
            URI uri = new URI(url);
            boolean valid = checkIfValidUrl(url);
            boolean robots = checkRobotRules(uri);
            LOGGER.info("Should visit {}: {} (valid) && {} (robots.txt).", url, valid, robots);
            result = valid && robots;
        } catch (URISyntaxException e) {
            result = false;
        }

        return result;
    }

    public boolean shouldDownloadDocument(String url){
        boolean result;
        try {
            URI uri = new URI(url);
            String[] parts = uri.getHost().split("\\.");
            result = !DISALLOWED_EXTENSIONS.contains(parts[parts.length - 1]);
        } catch (URISyntaxException e) {
            result = false;
        }

        return result;
    }

    public boolean isTheSameDomain(String home, String url){
        boolean result = true;
        try {
            if(home != null && url != null){
                String homeHost = new URI(home).getHost();
                String nextHost = new URI(url).getHost();

                if(homeHost != null && nextHost != null){
                    String[] firstUrl = homeHost.split("\\.");
                    String[] secondUrl = nextHost.split("\\.");

                    if(!getRootDomain(firstUrl).equals(getRootDomain(secondUrl))){
                        result = false;
                    }
                }
                else{
                    result = false;
                }
            }
            else{
                result = false;
            }
        } catch (URISyntaxException e) {
            result = false;
        }

        if(!result){
            declinedUrlFromDomain++;
        }
        return result;

    }

    private boolean checkIfValidUrl(String url){
        return  url.startsWith("http://") || url.startsWith("https://");
    }

    private boolean checkRobotRules(URI url){
        boolean result = true;
        for(String rule : disallowRules){
            String path = url.getPath();
            if(path.startsWith(rule)){
                result = false;
                declinedUrlFromRobots++;
            }
        }

        return result;
    }

    private void initRobotRules(BufferedReader robotsBuffer) {
        String line;
        LOGGER.info("Robots.txt content:");
        try {
            while((line = robotsBuffer.readLine()) != null) {
                if(line.startsWith(DISALLOW_RULE)){
                    String rule = line.split(DISALLOW_RULE)[1];
                    disallowRules.add(rule);
                    LOGGER.info("\t[DISALLOW]: {}.", rule);
                }
            }
        } catch (IOException e) {
            LOGGER.error("Error during reading robots.txt: ", e);
        }
    }

    private static String getRootDomain(String[] domainsPart){
        StringBuilder result = new StringBuilder();
        for(int i = domainsPart.length - 1; i >=0 ; i--){
            result.append(domainsPart[i]);
            if(!COMMON_DOMAINS.contains(domainsPart[i])){
                break;
            }
        }
        return result.toString();
    }

    public int getRobotsRulesSize(){
        return this.disallowRules.size();
    }

    public int getDeclinedUrlFromRobots() {
        return declinedUrlFromRobots;
    }

    public int getDeclinedUrlFromDomain() {
        return declinedUrlFromDomain;
    }
}
