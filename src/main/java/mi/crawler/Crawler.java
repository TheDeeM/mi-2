package mi.crawler;

import mi.crawler.model.*;
import mi.crawler.utils.CrawlerThread;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

public class Crawler {
    private final Logger LOGGER = LoggerFactory.getLogger(getClass());

    private final static String ROBOTS_FILE = "/robots.txt";

    private CrawlerData crawlerData;
    private CrawlerRules crawlerRules;

    private String url;
    private String destination;

    private final List<String> links;
    private final HashSet<String> visited;

    private boolean working;
    private int threadsSleeping;
    private int linkToVisit = 0;

    public Crawler(String url, String destination) {
        crawlerData = new CrawlerData();
        this.url = url;
        this.destination = destination;
        this.visited = new HashSet<>();
        this.links = new ArrayList<>();
        this.working = false;
        this.crawlerRules = new CrawlerRules(downloadRobotsTxt());
    }

    public CrawlerData crawl(int threads, int maxSites) throws IOException, URISyntaxException {
        this.working = true;
        this.threadsSleeping = 0;

        crawlerData.init(url);
        CrawlerThread mainThread = new CrawlerThread(-1, url, crawlerRules);
        Site mainSite = mainThread.visitUrl(url, destination, links, visited);
        crawlerData.addSite(mainSite);

        LOGGER.debug(links.toString());

        LOGGER.info("Creating {} threads crawling sites...", threads);

        Thread[] workingThreads = new Thread[threads];
        for(int i = 0; i < threads; i++){
            workingThreads[i] = createNewCrawlingThread(i, threads, maxSites);
            workingThreads[i].start();
        }

        boolean done = false;
        while(!done){
            done = true;
            for(int i = 0; i < threads; i++){
                if(!Thread.State.TERMINATED.equals(workingThreads[i].getState())){
                    done = false;
                    break;
                }
            }
        }
        crawlerData.stop();

        LOGGER.info("Crawler for {} has done its work in {} ms).", url, crawlerData.getWorkingTime());
        LOGGER.info("{} sites were declined by robots.txt and {} were from other domain.",
                crawlerRules.getDeclinedUrlFromRobots(), crawlerRules.getDeclinedUrlFromDomain());

        return crawlerData;
    }

    public CrawlerStats getStats(){
        CrawlerStats stats = new CrawlerStats();
        stats.setWorkingTime(crawlerData.getWorkingTime());
        stats.setDisallowedBecauseOfRobotsTxt(crawlerRules.getDeclinedUrlFromRobots());
        stats.setNotInTheSameDomain(crawlerRules.getDeclinedUrlFromDomain());

        double averageLoadingTime = 0;
        long maxLoadTime = 0;
        long minLoadTime = Long.MAX_VALUE;
        long errors = 0;
        long downloadedPages = 0;
        for (Site site : crawlerData.getSites()) {
            averageLoadingTime += site.getLoadTime();
            if(site.getStatus() == 0){
                errors++;
            }
            if(site.getFilePath() != null){
                downloadedPages++;
            }
            if(maxLoadTime < site.getLoadTime()){
                maxLoadTime = site.getLoadTime();
            }
            if(minLoadTime > site.getLoadTime()){
                minLoadTime = site.getLoadTime();
            }
        }

        stats.setAverageDownloadTime(averageLoadingTime / crawlerData.getSites().size());
        stats.setErrorsDuringDownload(errors);
        stats.setDownloadedPages(downloadedPages);
        stats.setMinLoadTime(minLoadTime);
        stats.setMaxLoadTime(maxLoadTime);
        stats.setFoundRobotsRules(crawlerRules.getRobotsRulesSize());
        stats.setLeftToVisit(links.size() - linkToVisit);
        return stats;
    }

    private BufferedReader downloadRobotsTxt(){
        BufferedReader in = null;

        try {
            in = new BufferedReader(new InputStreamReader(new URL(url + ROBOTS_FILE).openStream()));
        } catch (IOException e) {
            LOGGER.error("Error during loading robots.txt file from {}.", url + ROBOTS_FILE,  e);
        }

        return in;
    }

    private Thread createNewCrawlingThread(int i, int threads, int maxSites) {
        return new Thread(() -> {
            CrawlerThread thread = new CrawlerThread(i, url, crawlerRules);

            while(this.working){
                String nextUrl = getNextUrlToRead();
                if(visited.contains(nextUrl)){
                    LOGGER.error("SOMETHING WENT WRONG AS {} IS ALREADY VISITED.", url);
                }
                Site site = thread.visitUrl(nextUrl, destination, links, visited);
                if(site != null){
                    crawlerData.addSite(site);
                }

                checkIfShouldEnd(thread, nextUrl, threads, maxSites);
                LOGGER.info("Visited {} site(s) and there are {} more to visit.", crawlerData.getSites().size(), links.size() - linkToVisit);
            }
            LOGGER.info("Thread #{} has ended successfully.", i);
        });
    }

    private synchronized String getNextUrlToRead() {
        String result = null;

        if(links.size() > 0 && links.size() > linkToVisit){
            result = links.get(linkToVisit);
            linkToVisit++;
        }

        return result;
    }

    private synchronized void checkIfShouldEnd(CrawlerThread thread, String url, int threads, int maxSites) {
        CrawlerThreadState state = thread.getThreadState(url);

        if(CrawlerThreadState.GOING_TO_SLEEP.equals(state)){
            threadsSleeping++;
        }
        else if(CrawlerThreadState.WAKING_UP.equals(state)){
            threadsSleeping--;
        }

        if(this.working && (this.threadsSleeping == threads || this.visited.size() == maxSites)){
            this.working = false;
            LOGGER.info("Since all threads are sleeping, crawler is ending its work.");
        }
    }
}