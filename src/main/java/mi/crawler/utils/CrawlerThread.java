package mi.crawler.utils;

import mi.crawler.model.CrawlerRules;
import mi.crawler.model.CrawlerThreadState;
import mi.crawler.model.Site;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

public class CrawlerThread {
    private final Logger LOGGER = LoggerFactory.getLogger(getClass());

    private final String home;
    private int number;
    private boolean sleeping;
    private CrawlerRules crawlerRules;

    public CrawlerThread(int number, String home, CrawlerRules crawlerRules){
        this.number = number;
        this.sleeping = false;
        this.home = home;
        this.crawlerRules = crawlerRules;
        LOGGER.info("[THREAD {}] Initialized successfully.", number);
    }

    public Site visitUrl(String url, String destination, List<String> links, HashSet<String> visited){
        Site site = null;
        if(url != null){
            LOGGER.info("[THREAD {}] Visiting URL: {}.", number, url);
            site = getSite(url, destination);
            int newLinks = addNewLinksToVisit(site, links, visited);
            LOGGER.info("[THREAD {}] On URL {} found {} new from {} links.", number, url, newLinks, site.getLinks().size());
            visited.add(url);
        }

        return site;
    }

    public CrawlerThreadState getThreadState(String url){
        CrawlerThreadState state;
        if(url == null){
            state = sleeping ? CrawlerThreadState.SLEEPING : CrawlerThreadState.GOING_TO_SLEEP;
            sleeping = true;
            LOGGER.info("[THREAD {}] Nothing to do - going to sleep.", number);
        }
        else{
            state = sleeping ? CrawlerThreadState.WAKING_UP : CrawlerThreadState.WORKING;
            sleeping = false;
            LOGGER.info("[THREAD {}] Waking up to download {}.", number, url);
        }
        return state;
    }

    private synchronized int addNewLinksToVisit(Site site, List<String> links, HashSet<String> visited) {
        int newLinks = 0;
        String url = site.getUrl();

        List<String> l = site.getLinks();
        for(String link : l){
            if(!visited.contains(link) && !links.contains(link) && !url.equals(link)){
                LOGGER.debug("[THREAD {}] Adding link {}.", number, link);
                links.add(link);
                newLinks++;
            }
        }

        return newLinks;
    }

    private Site getSite(String url, String destination){
        Site site = new Site(url);

        if(crawlerRules.shouldDownloadDocument(url)){
            Elements elements = downloadPage(url, site, destination);

            if(elements != null){
                for (Element page : elements) {
                    String nextUrl = normalizeUrl(page.attr("abs:href"));
                    if(crawlerRules.isTheSameDomain(nextUrl, home) && crawlerRules.shouldVisitUrl(nextUrl)) {
                        site.addLink(nextUrl);
                    }
                }
            }
        }
        else{
            site.setStatus(204);
            LOGGER.info("[THREAD {}] Skipping downloading URL {}.", number, url);
        }

        site.loaded();
        return site;
    }

    private String normalizeUrl(String url){
        return  url.split("#")[0];
    }

    private Elements downloadPage(String url, Site site, String destination) {
        Document document;
        try {
            Connection connection = Jsoup.connect(url);
            document = connection.get();

            Connection.Response response = connection.response();
            site.setStatus(response.statusCode());
            site.setMimeType(response.contentType());

            if(document != null){
                String localFile = CrawlerUtils.saveDocument(document.html(), destination, url);
                site.setFilePath(localFile);
                LOGGER.info("[THREAD {}] Saved HTML from {} in {}.", number, url, localFile);
                return document.select("a[href]");
            }
        } catch (IOException e) {
            LOGGER.error("[THREAD {}] Error during loading url {}.", number, url, e);
        }

        return null;
    }
}
