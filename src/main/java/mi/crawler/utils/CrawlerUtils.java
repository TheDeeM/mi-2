package mi.crawler.utils;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Arrays;
import java.util.List;

public class CrawlerUtils {
    public static String saveDocument(String html, String destination, String url) throws IOException {
        String path = getLocalSitePath(destination, url);
        saveDocumentTo(html, path);
        return path;
    }

    private static void saveDocumentTo(String html, String path) throws IOException {
        File file = new File(path);
        file.getParentFile().mkdirs();
        file.createNewFile();
        PrintWriter pw = new PrintWriter(file);
        pw.write(html);
        pw.close();
    }

    private static String getLocalSitePath(String destination, String url) throws MalformedURLException {
        StringBuilder sb = new StringBuilder();
        sb.append(destination);
        String base = getSubUrl(url);

        if('/' != destination.charAt(destination.length() - 1) && (base == null || base.charAt(0) != '/')){
            sb.append("/");
        }

        sb.append(base);
        if(base.endsWith("/")){
            sb.append("index.html");
        }
        else if(!base.contains(".htm")){
            sb.append(".html");
        }

        return sb.toString();
    }

    private static String getSubUrl(String fullUrl) throws MalformedURLException {
        URL url = new URL(fullUrl);
        String base = url.getHost() + "/" + url.getPath();
        if(base.length() == 0){
            base = "index";
        }
        return base;
    }
}
