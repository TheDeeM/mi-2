package mi.analyzer;

import mi.analyzer.model.Node;
import mi.analyzer.model.WebGraph;
import mi.crawler.model.CrawlerData;
import mi.crawler.model.Site;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class WebAnalyzer {
    private final Logger LOGGER = LoggerFactory.getLogger(getClass());

    public WebGraph analyzeData(CrawlerData data){
        LOGGER.info("Web analyzer will analyze {} sites.", data.getSites().size());
        WebGraph webGraph = translateCrawlerData(data);
        LOGGER.info("Web analyzer found {} nodes.", webGraph.getNodes().size());
        return webGraph;
    }

    private WebGraph translateCrawlerData(CrawlerData data){
        List<Site> sites = data.getSites();
        return translateAllSites(data);
    }

    private WebGraph translateAllSites(CrawlerData data){
        List<Site> sites = data.getSites();

        LOGGER.info("Translating received sites to graph format...");
        WebGraph webGraph = new WebGraph();
        for(Site site : sites){
            String url = site.getUrl();
            if(site.getStatus() != 0){
                Node node = getOrCreate(url, webGraph);

                if(site.getLinks() != null){
                    for(String link : site.getLinks()){
                        Site nextSite = data.findSite(link);
                        if(nextSite != null && nextSite.getStatus() != 0){
                            Node nextNode = getOrCreate(link, webGraph);

                            LOGGER.debug("Node {} has connection with {}.", site.getUrl(), nextNode.getUrl());
                            nextNode.addInConnection(node);
                            node.addOutConnection(nextNode);
                        }
                        else{
                            LOGGER.info("Skipping node {} because it was not visited (status {}).", link, nextSite != null ? nextSite.getStatus(): null);
                        }
                    }
                }
            }
            else{
                LOGGER.info("Skipping site {} because it was loaded with error.", url);
            }
        }
        LOGGER.info("Translating received sites to graph format... END.");
        return webGraph;
    }

    private Node getOrCreate(String url, WebGraph webGraph){
        Node node = webGraph.findNode(url);
        if(node == null){
            node = new Node(url);
            webGraph.addNode(node);
        }
        return node;
    }
}
