package mi.analyzer.model;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.io.IOException;
import java.util.*;

import static com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility.ANY;
import static com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility.NONE;

@JsonAutoDetect(fieldVisibility = ANY, getterVisibility = NONE, setterVisibility = NONE)
public class WebGraph {
    private List<Node> nodes;

    public WebGraph(){
        nodes = new ArrayList<>();
    }

    public Node findNode(String url){
        Node result = null;
        if(url != null){
            for(Node node : nodes){
                if(url.equals(node.getUrl())){
                    result = node;
                    break;
                }
            }
        }
        return result;
    }

    public void addNode(Node node){
        this.nodes.add(node);
    }

    public List<Node> getNodes() {
        return nodes;
    }

    public void setNodes(List<Node> nodes) {
        this.nodes = nodes;
    }

    public void saveAsJson(String path) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        mapper.writerWithDefaultPrettyPrinter().writeValue(new File(path), this);
    }
}
