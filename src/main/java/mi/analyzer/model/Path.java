package mi.analyzer.model;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;

import static com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility.NONE;

@JsonAutoDetect(fieldVisibility = NONE, getterVisibility = NONE, setterVisibility = NONE)
public class Path {
    private List<Node> nodes;

    public Path(){
        nodes = new ArrayList<>();
    }

    public Path(Node node) {
        nodes = new ArrayList<>();
        nodes.add(node);
    }

    public Path(Path parent, Node node){
        nodes = new ArrayList<>(parent.nodes);
        nodes.add(node);
    }

    public Node getLastNode(){
        return this.nodes.get(this.nodes.size() - 1);
    }

    public void addNode(Node node){
        this.nodes.add(node);
    }

    public void addFirstNode(Node node){
        this.nodes.add(0, node);
    }

    public int getSize(){
        return this.nodes.size();
    }

    @JsonProperty("nodes")
    public List<String> getNodesUrl(){
        List<String> result = new ArrayList<>();
        for (Node node : nodes) {
            result.add(node.getUrl());
        }
        return result;
    }

    @JsonProperty("size")
    public int pathSize(){
        return nodes.size();
    }
}
