package mi.analyzer.model;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.io.IOException;

import static com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility.NONE;

@JsonAutoDetect(fieldVisibility = NONE, getterVisibility = NONE, setterVisibility = NONE)
public class PairOfNodes {
    private Node nodeA;
    private Node nodeB;
    @JsonProperty("start")
    private String nodeAUrl;
    @JsonProperty("end")
    private String nodeBUrl;
    @JsonProperty("path")
    private Path shortestPathBetween;

    public PairOfNodes(Node a, Node b, Path path){
        this.nodeA = a;
        this.nodeB = b;
        nodeAUrl = a.getUrl();
        nodeBUrl = b.getUrl();
        this.shortestPathBetween = path;
    }

    public PairOfNodes(Node a, Node b){
        this.nodeA = a;
        this.nodeB = b;
        nodeAUrl = a.getUrl();
        nodeBUrl = b.getUrl();
    }

    public Node getNodeA() {
        return nodeA;
    }

    public void setNodeA(Node nodeA) {
        this.nodeA = nodeA;
    }

    public Node getNodeB() {
        return nodeB;
    }

    public void setNodeB(Node nodeB) {
        this.nodeB = nodeB;
    }

    @Override
    public boolean equals(Object other){
        if (other == null) return false;
        if (other == this) return true;
        if (!(other instanceof PairOfNodes)){
            return false;
        }
        PairOfNodes otherPair = (PairOfNodes) other;
        return nodeA.equals(otherPair.getNodeA()) && nodeB.equals(otherPair.getNodeB());
    }

    @Override
    public String toString(){
        return nodeA.getUrl() + " => " + nodeB.getUrl();
    }

    public Path getShortestPathBetween() {
        return shortestPathBetween;
    }

    public void setShortestPathBetween(Path shortestPathBetween) {
        this.shortestPathBetween = shortestPathBetween;
    }

    public void saveAsJson(String path) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        mapper.writerWithDefaultPrettyPrinter().writeValue(new File(path), this);
    }
}
