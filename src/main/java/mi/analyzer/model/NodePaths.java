package mi.analyzer.model;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility.NONE;

@JsonAutoDetect(fieldVisibility = NONE, getterVisibility = NONE, setterVisibility = NONE)
public class NodePaths {
    private Node start;
    @JsonProperty("paths")
    private List<Path> paths;

    public NodePaths(){}

    public NodePaths(Node node){
        this.start = node;
        paths = new ArrayList<>();
    }

    public void addPath(Path path){
        this.paths.add(path);
    }

    public Node getStart() {
        return start;
    }

    @JsonProperty("node")
    public String getNode(){
        return start.getUrl();
    }

    public void saveAsJson(String path) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        mapper.writerWithDefaultPrettyPrinter().writeValue(new File(path), this);
    }
}
