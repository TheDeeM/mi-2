package mi.analyzer.model;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;

import static com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility.ANY;
import static com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility.NONE;

@JsonAutoDetect(fieldVisibility = ANY, getterVisibility = NONE, setterVisibility = NONE)
public class Node {
    private String url;
    private List<Connection> in;
    private List<Connection> out;

    public Node(){}

    public Node(String url) {
        this.url = url;
        this.in = new ArrayList<>();
        this.out = new ArrayList<>();
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public List<Connection> getIn() {
        return in;
    }

    public List<Connection> getOut() {
        return out;
    }

    public void addOutConnection(Node node){
        addConnection(out, node);
    }

    public void addInConnection(Node node){
        addConnection(in, node);
    }

    private void addConnection(List<Connection> list, Node node){
        boolean found = false;
        for(Connection connection : list){
            if(connection.getNode().getUrl().equals(node.getUrl())){
                found = true;
                connection.increment();
                break;
            }
        }
        if(!found){
            Connection connection = new Connection(node);
            list.add(connection);
        }
    }

    @JsonProperty("inDegree")
    public int getInDegree() {
        return in.size();
    }

    @JsonProperty("outDegree")
    public int getOutDegree() {
        return out.size();
    }
}
