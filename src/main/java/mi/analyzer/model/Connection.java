package mi.analyzer.model;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonProperty;

import static com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility.NONE;

@JsonAutoDetect(fieldVisibility = NONE, getterVisibility = NONE, setterVisibility = NONE)
public class Connection {
    @JsonProperty("count")
    private int count;
    private Node node;
    @JsonProperty("node")
    private String url;

    public Connection(){}

    public Connection(Node node){
        this.count = 1;
        this.node = node;
        this.url = node.getUrl();
    }

    public void increment() {
        this.count++;
    }

    public Node getNode() {
        return node;
    }
}
