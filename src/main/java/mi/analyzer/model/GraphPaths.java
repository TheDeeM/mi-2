package mi.analyzer.model;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.util.*;

import static com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility.NONE;

@JsonAutoDetect(fieldVisibility = NONE, getterVisibility = NONE, setterVisibility = NONE)
public class GraphPaths {
    private final Logger LOGGER = LoggerFactory.getLogger(getClass());

    @JsonProperty("diameter")
    private Path diameter;
    @JsonProperty("averageDistance")
    private double averageDistance;
    @JsonProperty("distribution")
    private HashMap<Integer, Integer> distribution;

    private HashSet<PairOfNodes> shortestPaths;

    public GraphPaths(WebGraph webGraph){
        shortestPaths = new HashSet<>();
        distribution = new HashMap<>();
        averageDistance = 0;

        computeShortestPaths(webGraph);
        computeAdditionalInfos();
    }

    @JsonProperty("pathsCount")
    public int allPaths(){
        return this.shortestPaths.size();
    }

    private void computeAdditionalInfos() {
        for (PairOfNodes pair : shortestPaths) {
            Path path = pair.getShortestPathBetween();
            Node start = pair.getNodeA();
            Node end = pair.getNodeB();
            if(end != path.getLastNode()){
                path.addNode(end);
            }

            path.addFirstNode(start);

            if(diameter == null || diameter.getSize() < path.getSize()){
                diameter = path;
            }

            averageDistance += path.getSize();
        }

        averageDistance /= shortestPaths.size();
    }

    private void computeShortestPaths(WebGraph webGraph) {
        int i = 0;
        for (Node node : webGraph.getNodes()) {
            i++;
            LOGGER.info("Computing shortest paths for node {} ({}/{})...", node.getUrl(), i, webGraph.getNodes().size());
            HashSet<String> visitedNodes = new HashSet<>();
            visitedNodes.add(node.getUrl());
            Queue<Path> queue = new LinkedList<>();

            for(Connection out : node.getOut()){
                queue.add(new Path(out.getNode()));
            }

            while(queue.size() > 0){
                Path path = queue.poll();
                Node nextNode = path.getLastNode();
                PairOfNodes pairOfNodes = new PairOfNodes(node, nextNode, path);

                if(!visitedNodes.contains(nextNode.getUrl())){
                    addShortestPath(pairOfNodes, visitedNodes, queue);
                }
            }
            LOGGER.info("Computing shortest paths for node {} ({}/{})... END.", node.getUrl(), i, webGraph.getNodes().size());
        }
    }

    private void addShortestPath(PairOfNodes pairOfNodes, HashSet<String> visitedNodes, Queue<Path> queue) {
        visitedNodes.add(pairOfNodes.getNodeB().getUrl());
        shortestPaths.add(pairOfNodes);
        updateDistribution(pairOfNodes.getShortestPathBetween());

        for(Connection out : pairOfNodes.getNodeB().getOut()){
            queue.add(new Path(pairOfNodes.getShortestPathBetween(), out.getNode()));
        }
    }

    private void updateDistribution(Path path) {
        Integer count = distribution.get(path.getSize());
        if(count == null){
            count = 1;
        }
        distribution.put(path.getSize(), count + 1);
    }

    public List<NodePaths> getPaths(){
        LOGGER.info("Converting to list of node paths...");
        List<NodePaths> result = new ArrayList<>(shortestPaths.size());

        for(PairOfNodes pair : shortestPaths){
            Node node = pair.getNodeA();
            NodePaths np = findOrCreateNodePath(node, result);
            np.addPath(pair.getShortestPathBetween());
            result.add(np);
        }

        return result;
    }

    private NodePaths findOrCreateNodePath(Node node, List<NodePaths> result) {
        NodePaths res = null;
        for(NodePaths path : result){
            if(path.getStart().equals(node)){
                res = path;
                break;
            }
        }
        if(res == null){
            res = new NodePaths(node);
        }
        return res;
    }

    public void saveAsJson(String path) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        mapper.writerWithDefaultPrettyPrinter().writeValue(new File(path), this);
    }
}
