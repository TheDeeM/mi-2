package mi.analyzer.model;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility.ANY;
import static com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility.NONE;

@JsonAutoDetect(fieldVisibility = ANY, getterVisibility = NONE, setterVisibility = NONE)
public class WebStats {
    private final Logger LOGGER = LoggerFactory.getLogger(getClass());

    private int allNodes;
    private int allEdges;
    private int inEdges;
    private int outEdges;
    private Node maxInDegree;
    private Node maxOutDegree;
    private Node minInDegree;
    private Node minOutDegree;
    private HashMap<Integer, Integer> distributionOfInDegrees;
    private HashMap<Integer, Integer> distributionOfOutDegrees;

    public WebStats(WebGraph graph) {
        allNodes = graph.getNodes().size();
        allEdges = 0;
        inEdges = 0;
        outEdges = 0;
        distributionOfInDegrees = new HashMap<>();
        distributionOfOutDegrees = new HashMap<>();

        computeDegrees(graph);
    }

    public void saveAsJson(String path) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        mapper.writerWithDefaultPrettyPrinter().writeValue(new File(path), this);
    }

    private void computeDegrees(WebGraph graph) {
        LOGGER.info("Finding degrees stats in graph...");
        List<Node> nodes = graph.getNodes();
        if(nodes.size() > 0){
            for(Node node : nodes){
                int inDegree = node.getInDegree();
                int outDegree = node.getOutDegree();
                allEdges += inDegree + outDegree;
                inEdges += inDegree;
                outDegree += outDegree;

                if(maxInDegree == null || maxInDegree.getInDegree() < inDegree){
                    maxInDegree = node;
                }
                if(maxOutDegree == null || maxOutDegree.getOutDegree() < outDegree){
                    maxOutDegree = node;
                }
                if(minInDegree == null || minInDegree.getInDegree() > inDegree){
                    minInDegree = node;
                }
                if(minOutDegree == null || minOutDegree.getOutDegree() > outDegree){
                    minOutDegree = node;
                }
                
                Integer outDistribution = distributionOfOutDegrees.get(outDegree);
                if(outDistribution == null){
                    outDistribution = 0;
                }
                distributionOfOutDegrees.put(outDegree, outDistribution + 1);

                Integer inDistribution = distributionOfInDegrees.get(inDegree);
                if(inDistribution == null){
                    inDistribution = 0;
                }
                distributionOfInDegrees.put(inDegree, inDistribution + 1);
            }
        }

        LOGGER.info("Finding degrees stats in graph... End. MAXIN: {} ({}), MAXOUT: {} ({}), MININ: {} ({}), MINOUT: {} ({})",
                maxInDegree.getUrl(), maxInDegree.getInDegree(), maxOutDegree.getUrl(), maxOutDegree.getOutDegree(),
                minInDegree.getUrl(), minInDegree.getInDegree(), minOutDegree.getUrl(), minOutDegree.getOutDegree());
    }

    public int getAllNodes() {
        return allNodes;
    }
}
