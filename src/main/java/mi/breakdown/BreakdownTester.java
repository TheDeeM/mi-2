package mi.breakdown;

import mi.analyzer.model.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

public class BreakdownTester {
    private final Logger LOGGER = LoggerFactory.getLogger(getClass());
    private List<Node> nodes;
    private Node startNode;
    private WebGraph webGraph;

    public BreakdownTester(WebGraph webGraph, String startNode) {
        this.nodes = new ArrayList<>(webGraph.getNodes());
        this.startNode = webGraph.findNode(startNode);
        this.webGraph = webGraph;
    }

    public BreakdownTester(WebGraph webGraph, String startNode, boolean sortNodes) {
        this.nodes = new ArrayList<>(webGraph.getNodes());
        this.startNode = webGraph.findNode(startNode);
        this.webGraph = webGraph;

        if(sortNodes){
            this.nodes.sort((o1, o2) -> Integer.valueOf(o2.getOutDegree()).compareTo(o1.getOutDegree()));
        }
    }


    public boolean testIntegrity(){
        HashSet<String> visited = new HashSet<>();
        visited.add(startNode.getUrl());
        Queue<Node> queue = new LinkedList<>();

        for(Connection out : startNode.getOut()){
            queue.add(out.getNode());
        }

        while(queue.size() > 0){
            Node node = queue.poll();
            if(!visited.contains(node.getUrl())){
                if(nodes.contains(node)){
                    for (Connection connection : node.getOut()) {
                        queue.add(connection.getNode());
                    }
                }
                visited.add(node.getUrl());
            }
        }

        LOGGER.info("Difference between visited and existing: {}",  visited.size() - webGraph.getNodes().size());
        return true || visited.size() >= nodes.size();
    }

    public void removeRandomVertices(int verticesToRemove) {
        for(int i = 0; i < verticesToRemove; i++){
            Random random = new Random();
            int toRemove = random.nextInt(nodes.size());
            this.nodes.remove(toRemove);
        }
    }

    public int removeRandomUntilInvalid(){
        int i = 0;
        while(nodes.size() > 1 && testIntegrity()){
            Random random = new Random();
            int toRemove = random.nextInt(nodes.size());
            this.nodes.remove(toRemove);
            i++;
        }
        return i;
    }

    public int removeMaxDegreeUntilInvalid(){
        int i = 0;
        while(nodes.size() > 1 && testIntegrity()){
            this.nodes.remove(0);
            i++;
        }
        return i;
    }

    public Node getMaxNode(){
        return nodes.get(0);
    }
}
