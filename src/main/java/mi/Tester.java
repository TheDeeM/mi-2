package mi;

import com.fasterxml.jackson.databind.ObjectMapper;
import mi.analyzer.WebAnalyzer;
import mi.analyzer.model.*;
import mi.breakdown.BreakdownTester;
import mi.crawler.Crawler;
import mi.crawler.model.CrawlerData;
import mi.crawler.model.CrawlerStats;
import mi.ranking.PageRank;
import mi.ranking.PageRankNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;

public class Tester {
    private final static List<String> adressesToTest = Arrays.asList(
            "http://www.anglia.ac.uk/",
            "http://www.anglia.ac.uk/student-life/life-on-campus",
            "http://www.anglia.ac.uk/privacy-and-cookies", "https://my.anglia.ac.uk",
            "http://www.anglia.ac.uk/study/continuing-professional-development?page=3",
            "http://www.anglia.ac.uk/science-and-technology/about/life-sciences/animal-environmental-biology/about-us"
    );
    private final static String CRAWLER_RESULT_FILE = "/results/crawler.json";
    private final static String CRAWLER_STATS_FILE = "/results/crawler_stats.json";
    private final static String WEB_GRAPH_FILE = "/results/web_graph.json";
    private final static String WEB_GRAPH_STATS_FILE = "/results/web_graph_stats.json";
    private final static String WEB_GRAPH_PATHS_FILE = "/results/web_graph_paths.json";
    private final static String WEB_GRAPH_PATH_FILE = "/results/paths/web_graph%s.json";
    private final static String PAGE_RANK_FILE = "/results/page_rank/%s/page_rank_iteration_%s.json";

    private final Logger LOGGER = LoggerFactory.getLogger(getClass());
    private String url;
    private String destination;

    public Tester(String url, String destination) {
        this.url = url;
        this.destination = destination;
    }

    public WebGraph createWebAnalyzer(CrawlerData data) throws IOException {
        LOGGER.info("Creating web analyzer.");
        WebAnalyzer webAnalyzer = new WebAnalyzer();
        WebGraph webGraph = webAnalyzer.analyzeData(data);
        webGraph.saveAsJson(destination + WEB_GRAPH_FILE);
        return webGraph;
    }

    public CrawlerData reloadCrawlerDataFromFile() throws IOException {
        LOGGER.info("Loading crawled data before from  {}.", destination + CRAWLER_RESULT_FILE);
        ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(new File(destination + CRAWLER_RESULT_FILE), CrawlerData.class);
    }

    public CrawlerData crawlSite(int threads, int maxSites) throws IOException, URISyntaxException {
        LOGGER.info("Starting crawling site {} with {} threads (max sites: {}). Saving output to: {}.", url, threads, maxSites, destination);

        Crawler crawler = new Crawler(url, destination);
        CrawlerData data = crawler.crawl(threads, maxSites);
        data.saveJsonToFile(destination + CRAWLER_RESULT_FILE);
        CrawlerStats stats = crawler.getStats();
        stats.saveAsJson(destination + CRAWLER_STATS_FILE);
        return data;
    }

    public void computePR(int iterations, WebGraph webGraph) throws IOException {
        PageRank mainPageRank = computePageRank(webGraph, iterations, 0.0, destination);
        PageRank pr1 = computePageRank(webGraph, iterations, 0.1, destination);
        PageRank pr2 = computePageRank(webGraph, iterations, 0.2, destination);
        PageRank pr5 = computePageRank(webGraph, iterations, 0.5, destination);
        PageRank pr7 = computePageRank(webGraph, iterations, 0.7, destination);
        PageRank pr9 = computePageRank(webGraph, iterations, 0.9, destination);
        List<PageRankNode> simpleRanks = mainPageRank.getSimpleRanks();

        computeDifferenceBetweenPR(mainPageRank, simpleRanks, pr1);
        computeDifferenceBetweenPR(mainPageRank, simpleRanks, pr2);
        computeDifferenceBetweenPR(mainPageRank, simpleRanks, pr5);
        computeDifferenceBetweenPR(mainPageRank, simpleRanks, pr7);
        computeDifferenceBetweenPR(mainPageRank, simpleRanks, pr9);

        showAddressesData(adressesToTest, mainPageRank);
        showAddressesData(adressesToTest, pr1);
        showAddressesData(adressesToTest, pr2);
        showAddressesData(adressesToTest, pr5);
        showAddressesData(adressesToTest, pr7);
        showAddressesData(adressesToTest, pr9);
    }

    public void damageGraph(WebGraph webGraph){
        LOGGER.info("Testing graph integrity...");
        testRemovingUntilInvalid(webGraph, 0);
    }

    private void testRemovingUntilInvalid(WebGraph webGraph, int repeats){
        LOGGER.info("Testing removing vertices until graph is invalid");
        double averageRandomVerticesToDamageGraph = 0;

        int max = 0, min = Integer.MAX_VALUE;
        for(int i = 0; i < repeats; i++){
            LOGGER.info("Test number {} of {}...", i + 1, repeats);

            BreakdownTester removeRandomUntilValid = new BreakdownTester(webGraph, url);
            int j = removeRandomUntilValid.removeRandomUntilInvalid();
            averageRandomVerticesToDamageGraph += j;

            if(min > j){
                min = j;
            }
            if(max < j){
                max = j;
            }

            LOGGER.info("Graph was invalid after removing {} random vertices. ", j);
        }
        BreakdownTester removeMaxDegreeUntilInvalid = new BreakdownTester(webGraph, url, true);

        LOGGER.info("Vertices to destroy graph:");
        LOGGER.info("[RANDOM] Average: {}, MAX: {}, MIN: {}.", averageRandomVerticesToDamageGraph / repeats, max, min);
        Node maxDegree = removeMaxDegreeUntilInvalid.getMaxNode();
        LOGGER.info("[MAX DEGREE]: {} ({})", removeMaxDegreeUntilInvalid.removeMaxDegreeUntilInvalid(), maxDegree.getUrl());
    }

    private void showAddressesData(List<String> adressesToTest,  PageRank pr){
        LOGGER.info("\nTESTING ADDRESSES FOR TEXT: \n");
        List<PageRankNode> nodes = pr.getSimpleRanks();
        for(String address : adressesToTest){
            int rank = findPRNode(nodes, address);
            double prr = getPageRank(nodes, address);
            LOGGER.info("Test Address: {} dampening, url: {}, rank: {}, PR: {}", pr.getDampening(),
                    address, pr.getRanks().size() - rank, prr);
        }
    }

    private void computeDifferenceBetweenPR(PageRank mainPageRank, List<PageRankNode> simpleRanks, PageRank pageRank) {
        LOGGER.info("Computing differences between PR {} and {}.", mainPageRank.getDampening(), pageRank.getDampening());
        int difference = 0;

        List<PageRankNode> otherNodes = pageRank.getSimpleRanks();

        for(int i = 0; i < simpleRanks.size(); i++){
            PageRankNode node = simpleRanks.get(i);
            int other = findPRNode(otherNodes, node.getUrl());
            difference += Math.abs(i - other);
        }
        difference /= simpleRanks.size();

        LOGGER.info("DIFFERENCE BETWEEN {} and {} is {}", mainPageRank.getDampening(), pageRank.getDampening(), difference);
    }

    private PageRank computePageRank(WebGraph webGraph, int iterations, double dampening,  String destination) throws IOException {
        LOGGER.info("Computing PageRank in {} iterations and dampening factor {}.", iterations, dampening);
        PageRank pageRank = new PageRank(webGraph, dampening);
        pageRank.saveAsJson(destination + String.format(PAGE_RANK_FILE, dampening, 0));
        for(int i = 0; i < iterations; i++){
            LOGGER.info("Iteration {} for Page Rank.", i + 1);
            pageRank.iterate();
            pageRank.saveAsJson(destination + String.format(PAGE_RANK_FILE, dampening, i + 1));
        }
        return pageRank;
    }

    public void computeStatsForWebGraph(WebGraph webGraph) throws IOException, URISyntaxException {
        LOGGER.info("Computing stats from web graph.");
        WebStats stats = new WebStats(webGraph);
        stats.saveAsJson(destination + WEB_GRAPH_STATS_FILE);
        LOGGER.info("Computing paths from web graph.");
        GraphPaths paths = new GraphPaths(webGraph);
        paths.saveAsJson(destination + WEB_GRAPH_PATHS_FILE);
        LOGGER.info("Saving found paths from web graph.");
        for(NodePaths pair : paths.getPaths()){
            pair.saveAsJson(destination + String.format(WEB_GRAPH_PATH_FILE, computePathForUrl(pair.getNode())));
        }
    }

    private String computePathForUrl(String url) throws URISyntaxException {
        URI uri = new URI(url);
        return uri.getHost() + "." +
                uri.getPath().replace('/', '_').replace('&', '_')
                        .replace('+', '_').replace('%', '_')
                        .replace('?', '_').replace('=', '_');
    }

    private int findPRNode(List<PageRankNode> otherNodes, String url) {
        int result = 0;
        for(int i = 0; i < otherNodes.size(); i++){
            if(otherNodes.get(i).getUrl().equals(url)){
                result = i;
                break;
            }
        }
        return result;
    }

    private double getPageRank(List<PageRankNode> nodes, String address) {
        double result = 0;
        for(int i = 0; i < nodes.size(); i++){
            if(nodes.get(i).getUrl().equals(address)){
                result = nodes.get(i).getRank();
                break;
            }
        }
        return result;
    }
}
